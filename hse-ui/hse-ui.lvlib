﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Dialogues" Type="Folder">
		<Item Name="Dialogue_Error.vi" Type="VI" URL="../Dialogues/Dialogue_Error.vi"/>
		<Item Name="Dialogue_OneButton.vi" Type="VI" URL="../Dialogues/Dialogue_OneButton.vi"/>
		<Item Name="Dialogue_Password.vi" Type="VI" URL="../Dialogues/Dialogue_Password.vi"/>
		<Item Name="Dialogue_String.vi" Type="VI" URL="../Dialogues/Dialogue_String.vi"/>
		<Item Name="Dialogue_TwoButton.vi" Type="VI" URL="../Dialogues/Dialogue_TwoButton.vi"/>
		<Item Name="Dialogue_UserCredentials.vi" Type="VI" URL="../Dialogues/Dialogue_UserCredentials.vi"/>
	</Item>
	<Item Name="FPControl" Type="Folder">
		<Item Name="FPControl.vi" Type="VI" URL="../FPControl/FPControl.vi"/>
		<Item Name="FPControl_Actions.ctl" Type="VI" URL="../FPControl/FPControl_Actions.ctl"/>
	</Item>
	<Item Name="GUI" Type="Folder">
		<Item Name="MultiColumnListbox_ColorRows.vi" Type="VI" URL="../GUI/MultiColumnListbox_ColorRows.vi"/>
		<Item Name="MultiColumnListbox_Resize.vi" Type="VI" URL="../GUI/MultiColumnListbox_Resize.vi"/>
		<Item Name="MulticolumnListControl__System.ctl" Type="VI" URL="../GUI/MulticolumnListControl__System.ctl"/>
		<Item Name="PanelUpdates_Defer.vi" Type="VI" URL="../GUI/PanelUpdates_Defer.vi"/>
		<Item Name="PanelUpdates_Enable.vi" Type="VI" URL="../GUI/PanelUpdates_Enable.vi"/>
		<Item Name="Window Color to Transparent.vi" Type="VI" URL="../GUI/Window Color to Transparent.vi"/>
		<Item Name="Window Handle.vi" Type="VI" URL="../GUI/Window Handle.vi"/>
	</Item>
	<Item Name="ProgressPopup" Type="Folder">
		<Item Name="ProgressPopup.vi" Type="VI" URL="../ProgressPopup/ProgressPopup.vi"/>
		<Item Name="ProgressPopup_Close.vi" Type="VI" URL="../ProgressPopup/ProgressPopup_Close.vi"/>
		<Item Name="ProgressPopup_Show.vi" Type="VI" URL="../ProgressPopup/ProgressPopup_Show.vi"/>
		<Item Name="ProgressPopup_Update.vi" Type="VI" URL="../ProgressPopup/ProgressPopup_Update.vi"/>
		<Item Name="ProgressPopupWithStatus.vi" Type="VI" URL="../ProgressPopup/ProgressPopupWithStatus.vi"/>
		<Item Name="ProgressPopupWithStatus_Close.vi" Type="VI" URL="../ProgressPopup/ProgressPopupWithStatus_Close.vi"/>
		<Item Name="ProgressPopupWithStatus_Notifiers.ctl" Type="VI" URL="../ProgressPopup/ProgressPopupWithStatus_Notifiers.ctl"/>
		<Item Name="ProgressPopupWithStatus_Show.vi" Type="VI" URL="../ProgressPopup/ProgressPopupWithStatus_Show.vi"/>
	</Item>
	<Item Name="Screens" Type="Folder">
		<Item Name="HSE_AboutScreen.vi" Type="VI" URL="../Screens/HSE_AboutScreen.vi"/>
		<Item Name="PROJECT_SplashScreen_Template.vi" Type="VI" URL="../Screens/PROJECT_SplashScreen_Template.vi"/>
		<Item Name="Screen_Event.ctl" Type="VI" URL="../Screens/Screen_Event.ctl"/>
		<Item Name="Screen_ShowHseAbout.vi" Type="VI" URL="../Screens/Screen_ShowHseAbout.vi"/>
		<Item Name="Screen_ShowProjectAbout.vi" Type="VI" URL="../Screens/Screen_ShowProjectAbout.vi"/>
	</Item>
</Library>
